FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get install -y --no-install-recommends \
        libssl-dev libz-dev libcairo-dev gir1.2-pango-1.0 libgirepository1.0-dev libacl1-dev liblz4-dev \
        tesseract-ocr-all libtesseract-dev libleptonica-dev libxxhash-dev \
        uwsgi-plugin-python3 python3-gdbm \
        libxmlsec1-dev mercurial git-svn libldap2-dev libldap-common libsasl2-dev librsvg2-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# renovate: datasource=pypi depName=weblate versioning=pep440
ARG WEBLATE_VERSION=5.10.3

RUN python3.12 -m venv /app/code/venv && \
    . /app/code/venv/bin/activate && \
    # pkgconfig required for borgbackup as dependency of weblate since 4.11
    pip3 install pkgconfig social-auth-core[openidconnect] && \
    pip3 install --no-binary cffi weblate[all]==${WEBLATE_VERSION} && \
    pip3 install gunicorn

RUN mv /app/code/venv/lib/python3.12/site-packages/weblate/settings_example.py /app/code/venv/lib/python3.12/site-packages/weblate/settings.py && \
    sed -e 's,^DATA_DIR = .*$,DATA_DIR = "/app/data/weblate/data",' \
        -e 's,^DEBUG = .*$,DEBUG = False,' \
        -e 's,^DEFAULT_LOGLEVEL = .*$,DEFAULT_LOGLEVEL = "INFO",' \
        -i /app/code/venv/lib/python3.12/site-packages/weblate/settings.py

# Configure nginx logs
RUN rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/weblate/supervisord.log /var/log/supervisor/supervisord.log

# Add uwsgi config
COPY weblate.ini /etc/uwsgi/apps-enabled/weblate.ini

# Prepare custom hooks
RUN echo -e "import site\nsite.addsitedir('/app/code/')\nsite.addsitedir('/app/data/')\n" >> /app/code/venv/lib/python3.12/site-packages/weblate/settings.py

# This will run /app/code/cloudron_settings.py
RUN echo -e 'try:\n\tfrom cloudron_settings import *\nexcept ImportError:\n\traise Exception("A cloudron_settings.py file is required to run this project")\n\n' >> /app/code/venv/lib/python3.12/site-packages/weblate/settings.py

RUN echo -e 'LANG="C.UTF-8"' > /etc/default/locale
ENV LC_ALL='C.UTF-8'

COPY cloudron_settings.py weblate.nginx start.sh /app/code/

CMD [ "/app/code/start.sh" ]
