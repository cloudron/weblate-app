# This file is sourced from /app/code/weblate-env/lib/python3.8/site-packages/weblate/settings.py
import os

# Postgres
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ["CLOUDRON_POSTGRESQL_DATABASE"],
        "USER": os.environ["CLOUDRON_POSTGRESQL_USERNAME"],
        "PASSWORD": os.environ["CLOUDRON_POSTGRESQL_PASSWORD"],
        "HOST": os.environ["CLOUDRON_POSTGRESQL_HOST"],
        "PORT": os.environ["CLOUDRON_POSTGRESQL_PORT"],
        "OPTIONS": {},
    }
}

# Redis
CACHES = {
    "default": {
        "BACKEND": "redis_lock.django_cache.RedisCache",
        "LOCATION": "redis://:" + os.environ["CLOUDRON_REDIS_PASSWORD"] + "@" + os.environ["CLOUDRON_REDIS_HOST"],
        # If redis is running on same host as Weblate, you might
        # want to use unix sockets instead:
        # "LOCATION": "unix:///var/run/redis/redis.sock?db=1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            # If you set password here, adjust CELERY_BROKER_URL as well
            "PASSWORD": None,
            "CONNECTION_POOL_KWARGS": {},
        },
        "KEY_PREFIX": "weblate",
        "TIMEOUT": 3600,
    },
    "avatar": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/app/data/weblate/avatar-cache",
        "TIMEOUT": 86400,
        "OPTIONS": {"MAX_ENTRIES": 1000},
    },
}

# OpenID Connect
if "CLOUDRON_OIDC_ISSUER" in os.environ:
    AUTHENTICATION_BACKENDS = (
        'social_core.backends.email.EmailAuth',
        'social_core.backends.open_id_connect.OpenIdConnectAuth',
        'weblate.accounts.auth.WeblateUserBackend',
    )

    SOCIAL_AUTH_OIDC_TITLE = os.environ["CLOUDRON_OIDC_PROVIDER_NAME"]
#    SOCIAL_AUTH_OIDC_IMAGE = ""
    SOCIAL_AUTH_OIDC_OIDC_ENDPOINT = os.environ["CLOUDRON_OIDC_ISSUER"]
    SOCIAL_AUTH_OIDC_KEY = os.environ["CLOUDRON_OIDC_CLIENT_ID"]
    SOCIAL_AUTH_OIDC_SECRET = os.environ["CLOUDRON_OIDC_CLIENT_SECRET"]
    SOCIAL_AUTH_OIDC_USERNAME_KEY = "sub"
    CSP_FORM_SRC = ["self", os.environ["CLOUDRON_OIDC_ISSUER"]]

    # only set if not yet specified in custom
    REGISTRATION_OPEN = False # this hides the 'Registration' link
    REGISTRATION_ALLOW_BACKENDS = ["username","email","oidc"] # allows us to add invite users manually

# Email settings
EMAIL_HOST = os.environ["CLOUDRON_MAIL_SMTP_SERVER"]
EMAIL_HOST_PASSWORD = os.environ["CLOUDRON_MAIL_SMTP_PASSWORD"]
EMAIL_HOST_USER = os.environ["CLOUDRON_MAIL_SMTP_USERNAME"]
EMAIL_PORT = int(os.environ["CLOUDRON_MAIL_SMTP_PORT"])

# Other
ENABLE_HTTPS = True
SITE_TITLE = "Weblate"
SITE_DOMAIN = os.environ["CLOUDRON_APP_DOMAIN"]
SITE_URL = "{}://{}".format("https" if ENABLE_HTTPS else "http", SITE_DOMAIN)
ALLOWED_HOSTS = [ os.environ["CLOUDRON_APP_DOMAIN"] ]

DEFAULT_FROM_EMAIL = os.environ["CLOUDRON_MAIL_FROM"]
SERVER_EMAIL = os.environ["CLOUDRON_MAIL_FROM"]

SECRET_KEY = os.environ["SECRET_KEY"]

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True

SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True
SECURE_HSTS_SECONDS = 3600
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True

# WebAuthn
OTP_WEBAUTHN_RP_NAME = SITE_TITLE
OTP_WEBAUTHN_RP_ID = SITE_DOMAIN.split(":")[0]
OTP_WEBAUTHN_ALLOWED_ORIGINS = [ os.environ["CLOUDRON_APP_ORIGIN"] ]
OTP_WEBAUTHN_ALLOW_PASSWORDLESS_LOGIN = False
OTP_WEBAUTHN_HELPER_CLASS = "weblate.accounts.utils.WeblateWebAuthnHelper"

DEFAULT_LOGLEVEL = "INFO" # <- overwritten in main file in Dockerfile so it may be overwritten by the user
DEFAULT_LOG = "console"
LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
    "formatters": {
        "syslog": {"format": "weblate[%(process)d]: %(levelname)s %(message)s"},
        "simple": {"format": "[%(asctime)s: %(levelname)s/%(process)s] %(message)s"},
        "logfile": {"format": "%(asctime)s %(levelname)s %(message)s"},
        "django.server": {
            "()": "django.utils.log.ServerFormatter",
            "format": "[%(server_time)s] %(message)s",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "django.server": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "django.server",
        }
    },
    "loggers": {
        "django.request": {
            "handlers": [DEFAULT_LOG],
            "level": "ERROR",
            "propagate": True,
        },
        "django.server": {
            "handlers": ["django.server"],
            "level": "INFO",
            "propagate": False,
        },
        "weblate": {"handlers": [DEFAULT_LOG], "level": DEFAULT_LOGLEVEL},
        # Logging VCS operations
        "weblate.vcs": {"handlers": [DEFAULT_LOG], "level": DEFAULT_LOGLEVEL},
        # Python Social Auth
        "social": {"handlers": [DEFAULT_LOG], "level": DEFAULT_LOGLEVEL},
        # Django Authentication Using LDAP
        "django_auth_ldap": {"handlers": [DEFAULT_LOG], "level": DEFAULT_LOGLEVEL},
        # SAML IdP
        "djangosaml2idp": {"handlers": [DEFAULT_LOG], "level": DEFAULT_LOGLEVEL},
    },
}

# Load user's config in the end to allow registration options to be overwritten
try:
    from custom_settings import *
except ImportError:
    raise Exception("/app/data/custom_settings.py could not be loaded or is missing")

